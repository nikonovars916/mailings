Для запуска в докере прописать команду:
docker compose up --build

Документация к API - http://127.0.0.1/docs

Альтернативная документация к API - http://127.0.0.1/docs/redoc

Логин админа: admin
Пароль админа: password

Выполненные дополнительные задания:
3, 5

Зависимости:

django = "^4.2.5"

djangorestframework = "^3.14.0"

drf-spectacular = "^0.26.4"

django-filter = "^23.3"

psycopg = "^3.1.10"

psycopg2 = "^2.9.7"

djangorestframework-simplejwt = "^5.3.0"

requests = "^2.31.0"

celery = "^5.3.4"

django-celery-beat = "^2.5.0"

redis = "^5.0.0"

gunicorn = "^21.2.0"
