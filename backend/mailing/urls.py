from django.urls import path
from mailing.views import *

urlpatterns = [
    path('', MailingListCreateAPIView.as_view()),
    path('<int:pk>', MailingRetrieveUpdateDestroyAPIView.as_view()),
    path('<int:mailing_id>/messages', MailingMessagesListAPIView.as_view()),
    path('message/<int:pk>', MailingMessageRetrieveAPIView.as_view()),
    path('active', MailingListCreateAPIView.as_view())
]
