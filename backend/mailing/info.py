import os

CREATED_MESSAGE_STATUS = 'created'
NOT_DELIVERED_STATUS = 'not_delivered'
DELIVERED_MESSAGE_STATUS = 'delivered'


THIRD_API_URL = os.getenv('THIRD_API_URL', None)
JWT_ACCESS_TOKEN = os.getenv('JWT_ACCESS_TOKEN', None)