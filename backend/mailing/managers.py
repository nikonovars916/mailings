from django.db import models
from django.db.models import Count, Q

from mailing.info import *


class MailingManager(models.Manager):

    def get_queryset(self, *args, **kwargs):
        qs = super().get_queryset()
        return qs.filter(*args, **kwargs).annotate(
            created_messages_count=Count(
                'messages',
                filter=Q(
                    messages__status=CREATED_MESSAGE_STATUS
                )
            ),
            delivered_messages_count=Count(
                'messages',
                filter=Q(
                    messages__status=DELIVERED_MESSAGE_STATUS
                )
            ),
            not_delivered_messages_count=Count(
                'messages',
                filter=Q(
                    messages__status=NOT_DELIVERED_STATUS
                )
            ),
            total_messages_count=Count('messages')
        )
