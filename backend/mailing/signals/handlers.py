from celery.result import AsyncResult
from django.db.models.signals import post_save
from django.dispatch import receiver
from mailing.models import Mailing
from mailing.tasks import run_mailing_task


@receiver(post_save, sender=Mailing)
def send_mailing_messages_if_valid_send_time(sender, **kwargs):
    """
    Запускаем рассылку сообщений, если текущее время удовлетворяет
    времени рассылки
    """

    mailing: Mailing = kwargs.get('instance', None)

    if mailing.is_time_valid():
        task = run_mailing_task.delay(mailing.pk)
        mailing.task_id = task.id
    else:
        mailing.terminate_task()
        task: AsyncResult = run_mailing_task.apply_async(
            kwargs={'mailing_pk': mailing.pk},
            countdown=mailing.get_timedelta().total_seconds()
        )
        mailing.task_id = task.id

