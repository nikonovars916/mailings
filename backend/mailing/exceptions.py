from rest_framework.exceptions import APIException


class TimeIsNotValidException(APIException):
    status_code = 400
    default_code = 400
    default_detail = 'Временной интервал проведения рассылки не находиться ниже текущего времени!'


class StartDateTimeGreaterEndDateTimeException(APIException):
    status_code = 400
    default_code = 400
    default_detail = 'Дата и время запуска не могут быть больше даты и времени окончания рассылки!'
