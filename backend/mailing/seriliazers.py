from rest_framework import serializers
from clients.serializers import ClientModelSerializer
from mailing.exceptions import *
from mailing.models import *


class MailingModelSerializer(serializers.ModelSerializer):
    created_messages_count = serializers.SerializerMethodField(
        'get_created_messages_count',
        read_only=True,
        label='Кол-во созданных сообщений для клиентов'
    )
    delivered_messages_count = serializers.SerializerMethodField(
        'get_delivered_messages_count',
        read_only=True,
        label='Кол-во доставленных в ходе рассылки сообщений для клиентов'
    )
    not_delivered_messages_count = serializers.SerializerMethodField(
        'get_not_delivered_messages_count',
        read_only=True,
        label='Кол-во недоставленных в ходе рассылки сообщений для клиентов'
    )
    total_messages_count = serializers.SerializerMethodField(
        'get_total_messages_count',
        read_only=True,
        label='Общее кол-во сообщений рассылки'
    )

    def validate(self, attrs):
        attrs = super().validate(attrs)
        start_datetime = attrs.get('start_datetime', None)
        end_datetime = attrs.get('end_datetime', None)
        now = datetime.now()
        if start_datetime < now or end_datetime < now:
            raise TimeIsNotValidException
        if start_datetime > end_datetime:
            raise StartDateTimeGreaterEndDateTimeException
        return attrs

    @staticmethod
    def get_total_messages_count(obj: Mailing) -> int:
        return obj.total_messages_count if hasattr(obj, 'total_messages_count') \
            else obj.get_total_messages_count()

    @staticmethod
    def get_created_messages_count(obj: Mailing) -> int:
        return obj.created_messages_count if hasattr(obj, 'created_messages_count')\
            else obj.get_created_messages_count()

    @staticmethod
    def get_delivered_messages_count(obj: Mailing) -> int:
        return obj.delivered_messages_count if hasattr(obj, 'delivered_messages_count')\
            else obj.get_delivered_messages_count()

    @staticmethod
    def get_not_delivered_messages_count(obj: Mailing) -> int:
        return obj.not_delivered_messages_count if hasattr(obj, 'not_delivered_messages_count') \
            else obj.get_not_delivered_messages_count()

    class Meta:
        model = Mailing
        fields = '__all__'


class MailingMessageModelSerializer(serializers.ModelSerializer):
    mailing = MailingModelSerializer(
        many=False,
        read_only=True,
        label='Рассылка'
    )
    client = ClientModelSerializer(
        many=False,
        read_only=True,
        label='Клиент'
    )

    class Meta:
        model = MailingMessage
        fields = '__all__'
        extra_kwargs = {
            'status': {
                'read_only': True
            }
        }
