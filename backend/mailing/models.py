from datetime import timedelta
from typing import Self
import requests
from django.core.validators import MinLengthValidator
from django.db import models
from django.db.models import QuerySet, Q, F
from django.utils.datetime_safe import datetime
from rest_framework import status
from backend.celery import app
from clients.models import Client
from mailing.info import *
from mailing.managers import MailingManager


class Mailing(models.Model):
    """Модель предназначена для работы с рассылками"""
    start_datetime: datetime = models.DateTimeField(
        verbose_name='Дата и время запуска рассылки'
    )
    text: str = models.TextField(
        verbose_name='Текст сообщения для клиента'
    )
    end_datetime: datetime = models.DateTimeField(
        verbose_name='Дата и время окончания рассылки'
    )
    client_operator_code = models.CharField(
        verbose_name='Код мобильного оператора клиентов',
        max_length=3,
        validators=[
            MinLengthValidator(3)
        ]
    )
    client_tag = models.CharField(
        verbose_name='Тэг фильтрации клиентов для рассылки',
        max_length=255
    )
    task_id = models.CharField(
        verbose_name='ID задачи в Celery',
        null=True,
        blank=True,
        editable=False
    )

    def get_timedelta(self) -> timedelta or None:
        """
        Получаем разницу между текущем датой и временем
        и датой и временем запуска рассылки
        """
        now = datetime.now()
        if self.start_datetime > now:
            return self.start_datetime - now

    def terminate_task(self):
        """Отменяем рассылку"""
        if self.task_id:
            app.control.revoke(self.task_id, terminate=True)

    objects = MailingManager()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=Q(end_datetime__gt=F('start_datetime')),
                name="end_datetime_gt_start_datetime_constraint",
                violation_error_message=f'Дата и время окончания рассылки не может быть меньше даты и времени запуска!'
            )
        ]

    def is_time_valid(self) -> bool:
        """Проверяем, является текущее или переданное время допустимы для рассылки"""
        return self.start_datetime <= datetime.now() <= self.end_datetime

    def get_mailing_clients(self) -> QuerySet[Client]:
        """Получаем отфильтрованных клиентов для рассылки"""
        return Client.objects.filter(
            tag=self.client_tag,
            operator_code=self.client_operator_code
        )

    def get_mailing_messages_for_sending(self):
        """
        Получаем все не отправленные или не дошедшие до клиента
        сообщения для рассылки
        """
        return MailingMessage.objects.filter(
            mailing=self
        ).exclude(
            status=DELIVERED_MESSAGE_STATUS
        )

    def create_and_get_mailing_messages_for_sending(self) -> QuerySet:
        """Создаём сообщения рассылки для отправки"""
        mailing_clients = self.get_mailing_clients()
        mailing_messages_to_create = []
        for client in mailing_clients:
            mailing_messages_to_create.append(
                MailingMessage(
                    client=client,
                    mailing=self
                )
            )
        if mailing_messages_to_create:
            MailingMessage.bulk_create_or_update(mailing_messages_to_create)
        return self.get_mailing_messages_for_sending()

    def run_mailing(self):
        """Запускаем рассылку если текущее время удовлетворяет времени рассылки"""
        if self.is_time_valid():
            while self.is_time_valid():
                mailing_messages: QuerySet[MailingMessage] = self.create_and_get_mailing_messages_for_sending()
                to_update_messages = []
                for message in mailing_messages:
                    message.send_message()
                    to_update_messages.append(
                        message
                    )
                if to_update_messages:
                    MailingMessage.bulk_create_or_update(to_update_messages)

    def get_created_messages_count(self) -> int:
        """Возвращает кол-во созданных для клиентов сообщений рассылки"""
        return MailingMessage.objects.filter(
            mailing=self,
            status=CREATED_MESSAGE_STATUS
        ).count()

    def get_delivered_messages_count(self) -> int:
        """Возвращает кол-во доставленных клиентам сообщений в ходе рассылки"""
        return MailingMessage.objects.filter(
            mailing=self,
            status=DELIVERED_MESSAGE_STATUS
        ).count()

    def get_not_delivered_messages_count(self) -> int:
        """Возвращает кол-во доставленных клиентам сообщений в ходе рассылки"""
        return MailingMessage.objects.filter(
            mailing=self,
            status=NOT_DELIVERED_STATUS
        ).count()

    def get_total_messages_count(self) -> int:
        """Возвращает общее кол-во сообщений рассылки"""
        return MailingMessage.objects.filter(
            mailing=self
        ).count()


class MailingMessage(models.Model):
    """
    Модель предназначена для работы с сообщениями, которые были разосланным
    клиентам, в ходе рассылки
    """
    STATUSES_CHOICES = [
        (CREATED_MESSAGE_STATUS, 'Создано'),
        (DELIVERED_MESSAGE_STATUS, 'Доставлено'),
        (NOT_DELIVERED_STATUS, 'Не доставлено'),
    ]

    mailing: Mailing = models.ForeignKey(
        to=Mailing,
        on_delete=models.CASCADE,
        verbose_name='Рассылка, в рамках которой было отправлено сообщение',
        related_name='messages'
    )
    client: Client = models.ForeignKey(
        Client,
        on_delete=models.CASCADE,
        verbose_name='Клиент, которому отправили сообщение',
        related_name='clients'
    )
    sent_at: datetime = models.DateTimeField(
        verbose_name='Дата и время отправки',
        null=True,
        blank=True
    )
    status: str = models.CharField(
        verbose_name='Статус',
        choices=STATUSES_CHOICES,
        default=CREATED_MESSAGE_STATUS,
        max_length=255
    )
    result = models.TextField(
        verbose_name='Ответ сервера',
        null=True,
        blank=True
    )

    def send_message(self, force_save=False):
        """Отправляем сообщение"""
        if self.mailing.is_time_valid() or not self.status == DELIVERED_MESSAGE_STATUS:
            data = {
                'id': self.pk,
                'phone': self.client.phone,
                'text': self.mailing.text
            }
            self.sent_at = datetime.now()
            try:
                response = requests.post(
                    data=data,
                    url=f'{THIRD_API_URL}/send/{self.pk}',
                    headers={
                        'Authorization': f'Bearer {JWT_ACCESS_TOKEN}'
                    }
                )
                self.result = response.text
                if response.status_code == status.HTTP_200_OK:
                    self.status = DELIVERED_MESSAGE_STATUS
                else:
                    self.status = NOT_DELIVERED_STATUS
            except Exception as ex:
                self.result = ex
                self.status = NOT_DELIVERED_STATUS
            if force_save:
                self.save()

    @classmethod
    def bulk_create_or_update(cls, objects: list[Self]):
        return cls.objects.bulk_create(
            objs=objects,
            update_conflicts=True,
            update_fields=['status', 'sent_at', 'result'],
            unique_fields=['client', 'mailing']
        )

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=['client', 'mailing'],
                name='client_mailing_constraint',
                violation_error_message='Сообщение для этого клиента уже существует')
        ]
