from django.core.exceptions import ObjectDoesNotExist

from backend.celery import app
from mailing.models import Mailing


@app.task
def run_mailing_task(mailing_pk: int):
    try:
        mailing = Mailing.objects.get(pk=mailing_pk)
        mailing.run_mailing()
    except ObjectDoesNotExist:
        pass
