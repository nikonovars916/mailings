from rest_framework import generics
from mailing.seriliazers import *


class MailingListCreateAPIView(generics.ListCreateAPIView):
    """Получение всех рассылок системы"""
    serializer_class = MailingModelSerializer
    queryset = Mailing.objects.all()


class ActiveMailingListAPIView(generics.ListAPIView):
    """Получение всех активных рассылок системы"""
    serializer_class = MailingModelSerializer

    def get_queryset(self):
        now = datetime.now()
        return Mailing.objects.filter(
            start_datetime__lt=now,
            end_datatime__gt=now
        )


class MailingRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Получение рассылки - GET
    Изменение рассылки - PUT/PATCH
    Удаление рассылки - DELETE
    """
    serializer_class = MailingModelSerializer
    queryset = Mailing.objects.all()


class MailingMessagesListAPIView(generics.ListAPIView):
    """Получение всех сообщений рассылки с фильтрацией по статусу"""
    serializer_class = MailingMessageModelSerializer
    filterset_fields = ['status', ]

    def get_queryset(self):
        return MailingMessage.objects.filter(
            mailing__pk=self.kwargs.get('mailing_id', None)
        )


class MailingMessageRetrieveAPIView(generics.RetrieveAPIView):
    """Получение сообщения рассылки по ID"""
    serializer_class = MailingMessageModelSerializer
    queryset = MailingMessage.objects.all()

