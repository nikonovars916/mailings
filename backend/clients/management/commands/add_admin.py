from django.contrib.auth import get_user_model
from django.core.management.base import BaseCommand

from backend import settings

User = get_user_model()


class Command(BaseCommand):
    """
    Создаёт Создаёт суперпользователя с паролем и электронной почтой,
    заданой в переменных окружения
    """
    help = 'Создаёт суперпользователя'

    def handle(self, *args, **kwargs):
        username = settings.ADMIN_USERNAME
        password = settings.ADMIN_PASSWORD

        user = User.objects.filter(username=username).first()
        if not user:
            User.objects.create_superuser(
                username=username,
                password=password,
            )
            print("Created superuser with \n"
                  f"username:{username} \n"
                  f"password: {password}")
