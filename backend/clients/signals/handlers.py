from django.db.models.signals import pre_save
from django.dispatch import receiver
from clients.models import Client


@receiver(pre_save, sender=Client)
def set_mobile_operator_code(sender, **kwargs):
    """Устанавливаем код оператора"""

    client: Client = kwargs.get('instance', None)
    client.operator_code = client.phone[1:4]

