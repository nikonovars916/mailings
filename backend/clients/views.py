from rest_framework import generics

from clients.models import Client
from clients.serializers import ClientModelSerializer


class ClientListCreateAPIView(generics.ListCreateAPIView):
    """Список всех клиентов системы"""
    serializer_class = ClientModelSerializer
    queryset = Client.objects.all()


class ClientRetrieveUpdateDestroyAPIView(generics.RetrieveUpdateDestroyAPIView):
    """
    Получение клиента - GET
    Изменение клиента - PUT/PATCH
    Удаление клиента - DELETE
    """
    serializer_class = ClientModelSerializer
    queryset = Client.objects.all()
