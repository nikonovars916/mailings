from rest_framework import serializers
from clients.models import Client


class ClientModelSerializer(serializers.ModelSerializer):

    class Meta:
        model = Client
        fields = '__all__'
        extra_kwargs = {
            'operator_code': {
                'read_only': True
            }
        }
