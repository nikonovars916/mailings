from django.core.validators import (MinLengthValidator,
                                    MaxValueValidator,
                                    MinValueValidator)
from django.db import models


class Client(models.Model):
    phone: str = models.CharField(
        verbose_name='Номер телефона',
        max_length=11,
        unique=True,
        validators=[
            MinLengthValidator(11)
        ]
    )
    operator_code: str = models.CharField(
        verbose_name='Код мобильного оператора',
        max_length=3,
        validators=[
            MinLengthValidator(3)
        ],
        null=True,
        blank=True
    )
    tag: str = models.CharField(
        verbose_name='Тег (произвольная метка)',
        max_length=255
    )
    timezone: str = models.IntegerField(
        verbose_name='Часовой пояс',
        validators=[
            MinValueValidator(-12),
            MaxValueValidator(12)
        ]
    )

