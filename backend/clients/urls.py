from django.urls import path
from clients.info import *
from clients.views import *

urlpatterns = [
    path(
        '',
        ClientListCreateAPIView.as_view(),
        name=CLIENTS_LIST_CREATE_URL
    ),
    path(
        '<int:pk>',
        ClientRetrieveUpdateDestroyAPIView.as_view(),
        name=CLIENTS_RETRIEVE_UPDATE_DESTROY_URL
    ),
]
