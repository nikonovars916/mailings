from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('', include('swagger.urls')),
    path('admin/', admin.site.urls),
    path('api/auth/', include('rest_framework.urls')),
    path('api/mailings/', include('mailing.urls')),
    path('api/clients/', include('clients.urls')),
]
